import { defineStore } from 'pinia'

export interface ComponentData {
  props: { [key: string]: any }
  id: string
  name: string
}

export interface EditorProps {
  // 供中间编辑器渲染的数组
  components: ComponentData[]
  // 当前编辑器渲染的是哪个元素, uuid
  currentElement: string
}

export const useEditorStore = defineStore('editor', {
  state: (): EditorProps => {
    return {
      components: [],
      currentElement: ''
    }
  }
})
