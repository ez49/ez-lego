import { defineStore } from 'pinia'

export interface UserProps {
  isLogin: boolean
  userName?: string
}

export const useUserStore = defineStore('user', {
  state: (): UserProps => {
    return {
      isLogin: false
    }
  },
  actions: {
    login() {
      this.isLogin = true
      this.userName = 'qy49'
    }
    // logout() {
    //     store.$reset()
    // }
  }
})
