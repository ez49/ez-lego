import { defineStore } from 'pinia'

export interface TemplateProps {
  id: number
  title: string
  coverImg: string
  author: string
  copiedCount: number
}
export interface TemplatesProps {
  data: TemplateProps[]
}

export const useTemplateStore = defineStore('templates', {
  state: (): TemplatesProps => {
    return {
      data: []
    }
  },
  getters: {
    getTemplateById(state) {
      return (id: number) => state.data.find((t) => t.id === id)
    }
  }
})
