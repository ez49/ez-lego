import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '@/views/HomeView.vue'
import IndexView from '@/views/IndexView.vue'
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'index',
      component: IndexView,
      children: [
        { path: '', name: 'home', component: HomeView, meta: { title: '欢迎来到慕课乐高' } }
        // {
        //   path: 'template/:id',
        //   name: 'template',
        //   component: TemplateDetail,
        //   meta: { title: '模版详情' }
        // },
        // {
        //   path: 'works',
        //   name: 'works',
        //   component: Works,
        //   meta: { title: '我的作品', requiredLogin: true }
        // }
      ]
    },
    {
      path: '/editor/:id',
      name: 'editor',
      component: () => import('../views/EditorView.vue'),
      meta: { requiredLogin: true, title: '编辑我的设计' }
    },
    {
      path: '/login',
      name: 'login',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/LoginView.vue'),
      meta: { redirectAlreadyLogin: true, title: '登录到慕课乐高', disableLoading: true }
    }
  ]
})

export default router
